# frozen_string_literal: true

require "fakturownia/version"
require "fakturownia/client"
require "dry/configurable"

module Fakturownia
  extend Dry::Configurable

  setting :company_domain, default: ENV.fetch("FAKTUROWNIA_COMPANY_DOMAIN", nil)&.strip
  setting :api_domain, default: ENV.fetch("FAKTUROWNIA_DOMAIN", "fakturownia.pl")&.strip
  setting :invoices_endpoint, default: "invoices.json"
  setting :invoice_endpoint, default: "invoices"
  setting :api_token, default: ENV.fetch("FAKTUROWNIA_API_TOKEN", nil)&.strip
end
