# frozen_string_literal: true

require "dry-struct"
require "fakturownia/symbolize_struct"
require "fakturownia/types"

module Fakturownia
  module Models
    class Position < Fakturownia::SymbolizeStruct
      attribute :id, Fakturownia::Types::Integer
      attribute :invoice_id, Fakturownia::Types::Integer
      attribute :name, Fakturownia::Types::String
      attribute :description, Fakturownia::Types::String
      attribute :price_net, Fakturownia::Types::JSON::Decimal
      attribute :quantity, Fakturownia::Types::JSON::Decimal
      attribute :total_price_gross, Fakturownia::Types::JSON::Decimal
      attribute :total_price_net, Fakturownia::Types::JSON::Decimal
      attribute :account_id, Fakturownia::Types::Integer
      attribute :created_at, Fakturownia::Types::JSON::Date.optional
      attribute :updated_at, Fakturownia::Types::JSON::Date.optional
      attribute :additional_info, Fakturownia::Types::String
      attribute :quantity_unit, Fakturownia::Types::String
      attribute :tax, Fakturownia::Types::String
      attribute :price_gross, Fakturownia::Types::JSON::Decimal
      attribute :price_tax, Fakturownia::Types::JSON::Decimal
      attribute :total_price_tax, Fakturownia::Types::JSON::Decimal
      attribute :kind, Fakturownia::Types::String.optional
      attribute :invoice_position_id, Fakturownia::Types::Integer.optional
      attribute :product_id, Fakturownia::Types::Integer.optional
      attribute :deleted, Fakturownia::Types::Bool
      attribute :discount, Fakturownia::Types::String.optional
      attribute :discount_percent, Fakturownia::Types::String.optional
      attribute :tax2, Fakturownia::Types::JSON::Decimal
      attribute :exchange_rate, Fakturownia::Types::JSON::Decimal
      attribute :accounting_tax_kind, Fakturownia::Types::String.optional
      attribute :code, Fakturownia::Types::String.optional
      attribute :additional_fields, Fakturownia::Types::JSON::Hash
    end
  end
end
