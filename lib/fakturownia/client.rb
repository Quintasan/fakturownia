# frozen_string_literal: true

require "addressable/template"
require "fakturownia/api/invoice"
require "fakturownia/api/invoices"

module Fakturownia
  class Client
    def initialize
      raise "You must provide an API key" if Fakturownia.config.api_token.nil?

      company_domain = Fakturownia.config.api_token.split("/")[1]

      raise "You must provde company domain" if company_domain.nil?

      Fakturownia.config.company_domain = company_domain
    end

    def invoice(id:, format: :json)
      url = invoice_uri_template.expand(
        "id": id,
        "format": format,
        "api_token": Fakturownia.config.api_token,
      ).then(&:to_s)

      case format
      when :pdf
        response = HTTParty.get(url)
        file_name = response
                    .headers["content-disposition"]
                    .split(";")
                    .last.split('"')
                    .last
        [file_name, response.body]
      else
        Fakturownia::API::Invoice.new.fetch(url)
      end
    end

    def invoices(period: "this_month", page: 1)
      url = invoices_uri_template.expand(
        "api_token": Fakturownia.config.api_token,
        "period": period,
        "page": page
      ).then(&:to_s)
      Fakturownia::API::Invoices.new.fetch(url)
    end

    private

    def full_api_path
      @full_api_path ||= "https://#{Fakturownia.config.company_domain}.#{Fakturownia.config.api_domain}"
    end

    def invoices_uri_template
      @invoices_uri_template ||= Addressable::Template.new("#{full_api_path}/#{Fakturownia.config.invoices_endpoint}{?api_token,period,page}")
    end

    def invoice_uri_template
      @invoice_uri_template ||= Addressable::Template.new("#{full_api_path}/#{Fakturownia.config.invoice_endpoint}/{id}.{format}{?api_token}")
    end
  end
end
