# frozen_string_literal: true

require "fakturownia/models/invoice"
require "httparty"
require "dry/monads"

module Fakturownia
  module API
    class Invoice
      include Dry::Monads[:result, :do]

      def fetch(url)
        response = yield get(url)
        hash = yield parse(response)
        invoice = yield wrap(hash)

        Success(invoice)
      end

      private

      def get(url)
        response = HTTParty.get(url)
        if response.code == 200
          Success(response)
        else
          Failure("API returned no body")
        end
      end

      def parse(response)
        hash = JSON.parse(response.body)
        Success(hash)
      rescue JSON::ParserError
        Failure("API returned invalid JSON")
      end

      def transform(item)
        Fakturownia::Models::Invoice.new(item)
      end

      def wrap(hash)
        item = transform(hash)
        Success(item)
      rescue Dry::Struct::Error => e
        puts e.message
        Failure("Couldn't transform received JSON")
      end
    end
  end
end
