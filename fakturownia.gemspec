# frozen_string_literal: true

require_relative 'lib/fakturownia/version'

Gem::Specification.new do |spec|
  spec.name = "fakturownia"
  spec.version = Fakturownia::VERSION
  spec.authors = ["Michał Zając"]
  spec.email = ["michal.zajac@gmail.com"]

  spec.summary = 'Ruby wrapper for Fakturownia invoicing API'
  spec.description = <<~DESC
    Ruby wrapper for Polish invoicing system which allows you to perform CRUD operations on your invoices
  DESC
  spec.homepage = "https://gitlab.com/Quintasan/fakturownia"
  spec.license = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")
  spec.metadata = {
    "homepage_uri" => spec.homepage,
    "documentation_uri" => "https://www.rubydoc.info/gems/fakturownia",
    "source_code_uri" => "https://gitlab.com/Quintasan/fakturownia",
    "bug_tracker_uri" => "https://gitlab.com/Quintasan/fakturownia/-/issues",
    "changelog_uri" => "https://gitlab.com/Quintasan/fakturownia/-/blob/master/CHANGELOG.md",
  }

  spec.files = Dir["lib/**/*"]
  spec.require_paths = ["lib"]
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }

  spec.extra_rdoc_files = Dir["README.md", "CHANGELOG.md", "LICENSE.txt"]

  spec.rdoc_options += [
    "--title", "Fakturownia - Ruby wrapper for Fakturownia API",
    "--main", "README.md",
    "--line-numbers",
    "--inline-source",
    "--quiet"
  ]

  spec.add_dependency 'addressable', '~> 2.7'
  spec.add_dependency "dry-configurable", "~> 1.3"
  spec.add_dependency "dry-monads", "~> 1.3"
  spec.add_dependency "dry-struct", "~> 1.0"
  spec.add_dependency "dry-types", "~> 1.2"
  spec.add_dependency "httparty"
  spec.add_dependency "json"
end
