# frozen_string_literal: true

require "spec_helper"

RSpec.describe Fakturownia::Client do
  subject(:client) { described_class.new }

  context "without API token" do
    before do
      Fakturownia.config.company_domain = "test"
      Fakturownia.config.api_token = nil
    end

    it "can't be created" do
      expect { client }.to raise_exception(RuntimeError)
    end
  end

  context "without company domain" do
    before do
      Fakturownia.config.company_domain = nil
      Fakturownia.config.api_token = "test"
    end

    it "can't be created" do
      expect { client }.to raise_exception(RuntimeError)
    end
  end

  context "with proper configuration" do
    before do
      Fakturownia.config.api_token = "test/test"
    end

    it "can be created" do
      expect(client).not_to be_nil
    end

    describe "#invoice" do
      before do
        stub_get("/invoices/#{id}.json?api_token=#{Fakturownia.config.api_token}", "invoice")
      end

      let(:id) { 1 }
      let(:request) { client.invoice(id: 1) }
      let(:value) { request.value! }

      it "returns a Success" do
        expect(request).to be_success
      end

      it "wraps result into Fakturownia::Models::Invoice" do
        expect(value).to be_an(Fakturownia::Models::Invoice)
      end
    end

    describe "#invoices" do
      before do
        stub_get("/invoices.json?api_token=#{Fakturownia.config.api_token}&page=1&period=this_month", "invoices")
      end

      let(:request) { subject.invoices }
      let(:value) { request.value }

      it "returns a Success" do
        expect(request).to be_success
      end

      it "wraps all elements into Fakturownia::Models::Invoice" do
        value = request.value!
        expect(value).to all(be_an(Fakturownia::Models::Invoice))
      end
    end
  end
end
