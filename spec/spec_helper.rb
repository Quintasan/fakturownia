# frozen_string_literal: true

require "bundler/setup"
require "webmock/rspec"
require "pry"
require "fakturownia"

def load_fixture(name)
  File.new(File.dirname(__FILE__) + "/support/response_stubs/#{name}.json")
end

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end

%i[get post put delete].each do |method|
  define_method "stub_#{method}" do |path, fixture, status_code = 200|
    stub_request(method, "https://#{Fakturownia.config.company_domain}.#{Fakturownia.config.api_domain}#{path}")
      .to_return(body: load_fixture(fixture), status: status_code)
  end

  define_method "a_#{method}" do |path|
    a_request(method, "https://#{Fakturownia.config.company_domain}.#{Fakturownia.config.api_domain}#{path}")
  end
end
